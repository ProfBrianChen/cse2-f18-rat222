/// Raymond To 9/11/2018 CSE 02 Arithmetic
/// The purpose of this program is to compute the cost of the items bought,
/// including the PA sales tax of 6%
//
public class Arithmetic{
  //  main method required for every Java program
  public static void main(String args []){
    // our input data
    int numPants = 3; // Number of pairs of pants
    double pantsPrice = 34.98;  // Cost per pair of pants
    int numShirts = 2;  // Number of sweatshirts
    double shirtPrice = 24.99;  // Cost per shirt
    int numBelts = 1; // Number of belts
    double beltPrice = 33.99;  // Cost per belt
    double paSalesTax = 0.06; // The tax rate
    // Total cost of each kind of item
    double totalCostOfPants = numPants * pantsPrice;  // Total cost of pants
    double totalCostOfShirts = numShirts * shirtPrice;  // Total cost of shirts
    double totalCostOfBelts = numBelts * beltPrice;  // Total cost of belts
    // Sales tax charged buying all of each kind of item
    double pantsSalesTax = (int)(paSalesTax * totalCostOfPants * 100) / 100;// Sales tax charged on pants
    double shirtsSalesTax = (int)(paSalesTax * totalCostOfShirts * 100) / 100; // Sales tax charged on shirts
    double beltsSalesTax = (int)(paSalesTax * totalCostOfBelts * 100) / 100; // Sales tax charged on belts
    // Total cost of purchases (before tax)
    double totalCostOfPurchases = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    // Total sales tax
    double totalSalesTax = (int)((pantsSalesTax + shirtsSalesTax + beltsSalesTax) * 100) / 100;
    // Total paid for this transaction, including sales tax.
    double totalPaid = totalCostOfPurchases + totalSalesTax;
    // printing output data
    System.out.println("The total cost of pants was $" + totalCostOfPants); // Prints total cost of pants
    System.out.println("The total cost of shirts was $" + totalCostOfShirts); // Prints total cost of shirts
    System.out.println("The total cost of belts was $" + totalCostOfBelts); // Prints total cost of belts
    System.out.println("The sales tax charged on pants was $" + pantsSalesTax); // Prints sales tax charged on pants
    System.out.println("The sales tax charged on shirts was $" + shirtsSalesTax); // Prints sales tax charged on shirts
    System.out.println("The sales tax charged on belts was $" + beltsSalesTax); // Prints sales tax charged on belts
    System.out.println("The total cost of purchases (before tax) was $" + totalCostOfPurchases);  // Prints total cost of purchases (before tax)
    System.out.println("The total sales tax was $" + totalSalesTax);  // Prints total sales tax
    System.out.println("The total paid for this transaction, including sales tax, was $" + totalPaid);  // Prints total paid for this transaction, including sales tax
  } // end of main method
} // end of class  
    