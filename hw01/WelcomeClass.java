//////////////
//// Raymond To 9/4/2018 CSE 02 Welcome Class
///
public class WelcomeClass{
  
  public static void main(String args []){
    ///prints "WELCOME" box to terminal window 
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    ///prints "RAT222" design to terminal window
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-R--A--T--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    ///prints tweet-length autobiographical statement
    System.out.println("Hi, my name is Raymond To. I was born in Allentown, PA.");
    System.out.println("I have previously attended Hiram W. Dodd Elementary School,");
    System.out.println("South Mountain Middle School, and Louis E. Dieruff High School.");
    System.out.println("Also, I like oranges.");
      
  }
  
}