/// Raymond To 11/13/2018 CSE 02 CSE2Linear
/// The purpose of this program is to create an array
/// and search for a specific value in it
//
import java.util.Random; // Required to use Random class
import java.util.Scanner; // Required to use Scanner class
public class CSE2Linear{
  // main method required for every java program
  public static void main(String args[]){
    int prevGrade = 0;
    int grade = 0;
    int[] array = new int[15];
    Scanner scan = new Scanner(System.in);
    int studentNum = 1;
    System.out.println("Enter 15 ascending ints for final grades in CSE2");
    while(studentNum < 16){
      System.out.println("Enter the final grade for student # " + studentNum + " :");
      if(scan.hasNextInt()){
        grade = scan.nextInt();
        if(grade < 0 || grade > 100){
          grade = 0;
          System.out.println("ERROR: int must be between 0-100");
        }
        else{
          if(grade < prevGrade){
            grade = 0;
            System.out.println("ERROR: int must be greater than or equal to the previous int");
          }
          else{
            array[studentNum - 1] = grade;
            prevGrade = grade;
            studentNum++;
          }
        }
      }
      else{
        scan.next();
        System.out.println("ERROR: an int must be entered");
      }
    }
    for(int i = 0; i < 15; i++){
      System.out.print(array[i] + " ");
    }
    System.out.println();
    System.out.println("Enter a grade to search for: ");
    int binTarget = scan.nextInt();
    binSearch(array, binTarget);
    scramble(array);
    System.out.println("Enter a grade to search for: ");
    int linTarget = scan.nextInt();
    linSearch(array, linTarget);
  } // end of main metohd
  // method used for binary search
  public static void binSearch(int[] array, int binTarget){
    int low = 0;
    int high = array.length - 1;
    int mid = 0;
    int iteration = 0;
    while(low <= high){
      iteration++;
      mid = (int)(high + low)/2;
      if(array[mid] > binTarget){
        high = mid - 1;
      }
      else{
        if(array[mid] < binTarget){
          low = mid + 1;
        }
        else{
          System.out.println(binTarget + " was found in the list with " + iteration + " iterations");
          break;
        }
      }
    }
    if(low > high){
      System.out.println(binTarget + " was not found in the list with " + iteration + " iterations");
    }
  }
  // method used for scrambling an array of ints and printing it
  public static void scramble(int[] array){
    Random randy = new Random();
    for(int i = 0; i < 420; i++){
      int temp = array[0];
      int x = randy.nextInt(array.length);
      array[0] = array[x];
      array[x] = temp;
    }
    System.out.println("Scrambled:");
    for(int j = 0; j < array.length; j++){
      System.out.print(array[j] + " ");
    }
    System.out.println();
  }
  // method used for linear search
  public static void linSearch(int[] array, int linTarget){
    int i = 0;
    for(;i < array.length; i++){
      if(array[i] == linTarget){
        System.out.println(linTarget + " was found in the list with " + (i + 1) + " iterations");
        break;
      }
    }
    if(i == array.length){
      System.out.println(linTarget + " was not found in the list with " + i + " iterations");
    }
  }
} // end of class