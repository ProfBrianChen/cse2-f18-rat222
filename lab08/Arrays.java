/// Raymond To 11/8/2018 CSE 02 Arrays
/// The purpose of this program is to create an array
/// and display the # of occurances of each integer in it
//
import java.util.Random; // Required to use Random class
public class Arrays{
  // main method required for every java program
  public static void main(String args[]){
    // Constructs the Random() object to generate random integers from 0-99
    Random randy = new Random();
    // Declaration and allocation of arrays
    int[] array1 = new int[100];
    int[] array2 = new int[100];
    // Assigns and prints out values of first array
    System.out.print("Array 1 holds the following integers:");
    for(int i = 0; i < 100; i++){
      array1[i] = randy.nextInt(100);
      System.out.print(" " + array1[i]);
    }
    System.out.println();
    // loop designed to give info about the first array
    for(int i = 0; i < 100; i++){
      // Assigns values to the second array based on the occurance of values in the first array
      for(int j = 0; j < 100; j++){
        if(array1[j] == i){
          array2[i]++;
        }
      }
      // Prints out how many times a value occurs in the first array
      if(array2[i] > 1){
        System.out.println(i + " occurs " + array2[i] + " times ");
      }
      else{
        if(array2[i] == 1){
          System.out.println(i + " occurs 1 time");
        }
      }
    }
  } // end of main method
} // end of class