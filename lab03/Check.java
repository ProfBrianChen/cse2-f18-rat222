/// Raymond To 9/13/2018 CSE 02 Check
/// The purpose of this program is to determine how much each person will pay
/// if the bill is split evenly, including tip.
//
import java.util.Scanner; // Required in order to use Scanner class
public class Check{
  // main method required for every java program
  public static void main(String args []){
    Scanner myScanner = new Scanner(System.in); // Declares and constructs an instance of myScanner to accept input
    // prints prompts to accept input data from user
    System.out.print("Enter the original cost of the check in the form xx.xx: "); // Prints prompt
    double checkCost = myScanner.nextDouble();  // for inputing original cost of the check
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); // Prints prompt
    double tipPercent = myScanner.nextDouble(); // for inputing percentage tip that will be paid
    tipPercent /= 100;  // We want to convert the percentage into a decimal value
    System.out.print("Enter the amount of people who went out to dinner: ");  // Prints prompt
    int numPeople = myScanner.nextInt();  // for inputing amount of people who went out to dinner
    // calculations for output data
    double totalCost = checkCost * (1 + tipPercent); // Stores  value of total cost of check
    double costPerPerson = totalCost / numPeople; // Stores  value of how much each person will pay
    int dollars, dimes, pennies;  // Dimes and pennies are used to store the amount of cents
    dollars = (int)costPerPerson; // Get the whole amount dropping decimal fraction
    //get dimes amount, e.g., 
    // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
    //  where the % (mod) operator returns the remainder
    //  after the division:   583%100 -> 83, 27%5 -> 2
    dimes = (int)(costPerPerson * 10) % 10; // Tenths place decimal value for costPerPerson
    pennies = (int)(costPerPerson * 100) % 10;  // Hundredths place decimal value for costPerPerson
    // prints output data
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);
  } // end of main method
} // end of class