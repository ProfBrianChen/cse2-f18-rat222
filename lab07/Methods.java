/// Raymond To 10/25/2018 CSE 02 Methods
/// The purpose of this program is to 
/// generate random sentences
//
import java.util.Random; // Required to use Random class
public class Methods{
  // main method required for every java program
  public static void main(String args[]){
    Random randomGenerator = new Random();
    String sub = subject();
    int actionNum = randomGenerator.nextInt(8);
    System.out.println("The " + adjective() + " " + sub + " " + verb() + " the " + adjective() + " " + object() + ".");
    while(actionNum > 0){
      if(randomGenerator.nextInt(2) == 1){
        System.out.println("The " + sub + " " + verb() + " the " + adjective() + " " + object() + ".");
      }
      else{
        System.out.println("It " + verb() + " the " + adjective() + " " + object() + ".");
      }
      actionNum--;
    }
    System.out.println("That " + sub + " " + verb() + " their " + subject() + "!");
  }
  public static String adjective(){
    String adj = "";
    Random randomGenerator = new Random(); 
    int randomInt = randomGenerator.nextInt(10);
    switch(randomInt){
      case 0:
        adj = "white";
        break;
      case 1:
        adj = "brown";
        break;
      case 2:
        adj = "black";  
        break;
      case 3:
        adj = "grey";
        break;
      case 4:
        adj = "red";
        break;
      case 5:
        adj = "orange";
        break;
      case 6:
        adj = "yellow";
        break;
      case 7:
        adj = "green";
        break;
      case 8:
        adj = "blue";
        break;
      case 9:
        adj = "purple";
        break;
    }
    return adj;
  }
  public static String subject(){
    String sub = "";
    Random randomGenerator = new Random(); 
    int randomInt = randomGenerator.nextInt(10);
    switch(randomInt){
      case 0:
        sub = "knight";
        break;
      case 1:
        sub = "archer";
        break;
      case 2:
        sub = "soldier";
      case 3:
        sub = "horse";
        break;
      case 4:
        sub = "wizard";
        break;
      case 5:
        sub = "assassin";
        break;
      case 6:
        sub = "viking";
        break;
      case 7:
        sub = "child";
        break;
      case 8:
        sub = "man";
        break;
      case 9:
        sub = "woman";
        break;
    } 
    return sub;
  }  
  public static String verb(){
    String ver = "";
    Random randomGenerator = new Random(); 
    int randomInt = randomGenerator.nextInt(10);
    switch(randomInt){
      case 0:
        ver = "punched";
        break;
      case 1:
        ver = "kicked";
        break;
      case 2:
        ver = "ate";
        break;    
      case 3:
        ver = "smashed";
        break;
      case 4:
        ver = "carried";
        break;
      case 5:
        ver = "pushed";
        break;
      case 6:
        ver = "liked";
        break;
      case 7:
        ver = "spew";
        break;
      case 8:
        ver = "threw";
        break;
      case 9:
        ver = "cleaned";
        break;
    } 
    return ver;
  }
  public static String object(){
    String obj = "";
    Random randomGenerator = new Random(); 
    int randomInt = randomGenerator.nextInt(10);
    switch(randomInt){
      case 0:
        obj = "sword";
        break;
      case 1:
        obj = "bow";
        break;
      case 2:
        obj = "spear";
        break;
      case 3:
        obj = "cart";
        break;
      case 4:
        obj = "stick";
        break;
      case 5:
        obj = "knife";
        break;
      case 6:
        obj = "club";
        break;
      case 7:
        obj = "ball";
        break;
      case 8:
        obj = "box";
        break;
      case 9:
        obj = "bread";
        break;  
    } 
    return obj;  
  }
}