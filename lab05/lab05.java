/// Raymond To 9/13/2018 CSE 02 lab05
/// The purpose of this program is to ask the user for course information
/// and loop prompts if invalid data is inputted
//
import java.util.Scanner; // Required in order to use Scanner class
public class lab05{
  // main method required for every java program
  public static void main(String args []){
    boolean Int = false;
    boolean string = false;
    Scanner scan = new Scanner(System.in);  // Declares and constructs an instance of myScanner to accept input
    System.out.print("Enter the course number: ");
    while(Int == false){
     if(scan.hasNextInt()){
      int courseNum = scan.nextInt();
       Int = true;
     }
     else{
      scan.next();
      System.out.print("Please enter an integer: ");
     } 
    }
    System.out.print("Enter the department name: ");
    while(string == false){
     if(scan.hasNext()){
      String departmentName = scan.next();
       string = true;
     }
     else{
      scan.next();
      System.out.print("Please enter a string: ");
     } 
    }
    //System.out.print("Enter the number of times the class meets per week: ");
    //scan.nextInt();
    //System.out.print("Enter the time the class starts in the form xxx (no colon): ");
    //scan.nextInt();
    //System.out.print("Enter the instructor name: ");
    //scan.next();
    //System.out.print("Enter the number of students in the class: ");
    //scan.nextInt();
  } // end of main method
} // end of class