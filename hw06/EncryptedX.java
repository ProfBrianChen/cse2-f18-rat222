/// Raymond To 10/23/2018 CSE 02 EncryptedX
/// The purpose of this program is to 
/// generate a secret message X
//
// Required to use Scanner class
import java.util.Scanner;
public class EncryptedX{
  // main method required for every java program
  public static void main(String args[]){
    // size is declared and given a value that will ensure the while loop will run at least once
    int size = -1;
    // declares and contructs scanner to accept inpu
    Scanner scan = new Scanner(System.in);
    // checks for valid input and repeats message prompt until it is given
    while(size < 0 || size > 100){
      // prints message prompt for input
      System.out.print("Please enter size of the square as an integer betwwen 0-100: ");
      // gives size the input value if it is of the correct type
      if(scan.hasNextInt()){
        size = scan.nextInt();
      }
      // discards input if it is an incorrect type
      else{
        scan.next();
      }
    }
    // generates secret message X based on input
    for(int i = 0; i < size; i++){
      // prints out one line of the secret message X
      for(int j = 0; j < size; j++){
        // prints out part of the hidden X
        if(j == i || j == size - i - 1){
          System.out.print(" ");
        }
        // prints out the surrounding stars
        else{
          System.out.print("*");
        }
      }
      // advances to next line to be printed
      System.out.println("");
    }
  } // end of main method
} // end of class