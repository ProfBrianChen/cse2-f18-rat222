Homework 6 EncryptedX

Total Score: 100

Compiles       20 pts YES

Comments    10 pts YES

Uses nested for loop properly     15 pts YES

Checks for between 0-100   5 pts YES

Program works  (shows some sort of grid-like output)            50 pts YES

Prints x instead of *	-5 pts N/A

Asks for 1-100 instead of 0-100	-5 pts N/A
