/// Raymond To 9/20/2018 CSE 02 Card Generator
/// The purpose of this program is to 
/// pick a random card from a standard 52 card deck
//
public class CardGenerator{
  // main method required for every java program
  public static void main(String args []){
    int cardValue = (int)(Math.random()*51) + 1;
    int cardNum1 = cardValue % 13;
    String cardNum2;
    String cardSuit;
    if( cardValue <= 13 ){
      cardSuit = "Diamonds";
    }
    else{
      if( cardValue <= 26){
        cardSuit = "Clubs";
      }
      else{
        if( cardValue <= 39){
          cardSuit = "Hearts";
        }
        else{
          cardSuit = "Spades";
        }
      }
    }
    if( cardNum1 == 1 ){
      cardNum2 = "Ace";
    }
    else{
      if( cardNum1 == 11){
        cardNum2 = "Jack";
      }
      else{
        if( cardNum1 == 12){
          cardNum2 = "Queen";
        }
        else{
          if(cardNum1 == 0){
            cardNum2 = "King";
          }
          else{
            cardNum2 = "None";
          }
        }
      }
    }
    if( cardNum1 >= 2 && cardNum1 <= 10){
    System.out.println("You picked the " + cardNum1 + " of " + cardSuit);
    }
    else{
      System.out.println("You picked the " + cardNum2 + " of " + cardSuit);
    }
  }
}