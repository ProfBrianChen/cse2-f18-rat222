/// Raymond To 11/13/2018 CSE 02 hw08
/// The purpose of this program is to shuffle an array
/// of cards and deal a hand using the shuffled deck
//
import java.util.Random;
import java.util.Scanner;
public class Shuffling{
  public static void main(String args[]){
    Scanner scan = new Scanner(System.in); 
    //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    int numCards = 5; 
    int again = 1; 
    int index = 51;
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
    } 
    printArray(cards); 
    shuffle(cards); 
    printArray(cards); 
    while(again == 1){
      if(numCards > index +1){
        for (int i=0; i<52; i++){ 
          cards[i]=rankNames[i%13]+suitNames[i/13]; 
        }
        shuffle(cards);
        index = 51;
      } 
      hand = getHand(cards,index,numCards); 
      printArray(hand);
      index = index - numCards;
      System.out.println("Enter a 1 if you want another hand drawn"); 
      again = scan.nextInt(); 
    } 
  } // end of main method
  // Prints out the array of cards
  public static void printArray(String[] list){
    for(int i = 0; i < list.length; i++){
      System.out.print(list[i] + " ");
    }
    System.out.println();
  }
  // Shuffles the array of cards
  public static void shuffle(String[] list){
    Random randy = new Random();
    for(int i = 0; i < 420; i++){
      String temp = list[0];
      int j = randy.nextInt(52);
      list[0] = list[j];
      list[j] = temp;
    }
    System.out.println("Shuffled");
  }
  // Returns an array of a hand of cards specified by numCards
  public static String[] getHand(String[] list, int index, int numCards){
    String[] list2 = new String[numCards];
    for(int i = 0; i < numCards; i++){
      list2[i] = list[index - i];
    }
    System.out.println("Hand");
    return list2;
  }
} // end of class