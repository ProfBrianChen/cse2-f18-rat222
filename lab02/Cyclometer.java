/// Raymond To 9/5/2018 CSE 02 Cyclometer
/// The purpose of this program is to calculate the distance and time of bicycle trips,
/// as well as the speed of the bicycle.
//
public class Cyclometer{
  //  main method required for every Java program
  public static void main(String args []){
    // our input data
    int secsTrip1 = 480;  // Stores # of seconds for Trip 1
    int secsTrip2 = 3220;  // Stores # of seconds for Trip 2
    int countsTrip1 = 1561;  // Stores # of counts (rotations) for Trip 1
    int countsTrip2 = 9037;  // Stores # of counts (rotations) for Trip 2
    // our intermediate variables and output data
    double wheelDiameter = 27.0,  // Stores diameter of bicycle wheel in inches
    PI = 3.14159,  // Stores value of pi with 6 sig figs
    feetPerMile = 5280,  // Stores # of feet per mile
    inchesPerFoot = 12,  // Stores # of inches per foot
    secondsPerMinute = 60;  // Stores # of seconds per minute
    double distanceTrip1, distanceTrip2,totalDistance;  // Stores total combined distance traveled for Trip 1 and Trip 2
    // prints output data for # of minutes and counts for each Trip
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had "
                        + countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had "
                        + countsTrip2 + " counts.");
    // calculations for trip distances
    distanceTrip1 = countsTrip1 * wheelDiameter * PI / inchesPerFoot / feetPerMile;  // Gives total distance in miles for Trip 1
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;  // Gives total distance in miles for Trip 2
    totalDistance = distanceTrip1 + distanceTrip2;  // Gives total combined distance in miles for Trip 1 and Trip 2
    // prints output data for trip distances
    System.out.println("Trip 1 was "+distanceTrip1+" miles");  // Prints # of miles for Trip 1
    System.out.println("Trip 2 was "+distanceTrip2+" miles");  // Prints # of miles for Trip 2
    System.out.println("The total distance was "+totalDistance+" miles");
  } // end of main method
} // end of class