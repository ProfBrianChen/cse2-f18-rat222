/// Raymond To 9/18/2018 CSE 02 Pyramid
/// The purpose of this program is to calculate
/// the volume of a pyramid whose dimensions are known
//
import java.util.Scanner; // Required in order to use Scanner class
public class Pyramid{
  // main method required for every java program
  public static void main(String args []){
    Scanner myScanner = new Scanner(System.in); // Declares and constructs an instance of myScanner to accept input
    // prints prompts to accept input data from user
    System.out.print("The square side of the pyramid is (input length): "); // prints prompt for inputing
    double pyramidBase = myScanner.nextDouble();  // the length of the pyramid base
    System.out.print("The height of the pyramid is (input height): "); // prints prompt for inputing
    double pyramidHeight = myScanner.nextDouble();  // the height of the pyramid
    // calculation of output data
    double pyramidVolume = pyramidBase * pyramidHeight * pyramidBase / 3;
    // prints output data
    System.out.println("The volume inside the pyramid is: " + pyramidVolume);
  } // end of main method
} // end of class  