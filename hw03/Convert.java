/// Raymond To 9/18/2018 CSE 02 Convert
/// The purpose of this program is to determine
/// how much rain a hurricane precipitated in a given area
//
import java.util.Scanner; // Required in order to use Scanner class
public class Convert{
  // main method required for every java program
  public static void main(String args []){
    Scanner myScanner = new Scanner(System.in); // Declares and constructs an instance of myScanner to accept input
    // prints prompts to accept input data from user
    System.out.print("Enter the affected area in acres: "); // prints prompt for inputing
    double affectedArea = myScanner.nextDouble(); // affected area in acres
    System.out.print("Enter the rainfall in the affected area in inches: ");  // prints prompt for
    double rainFall = myScanner.nextDouble(); // inputing rain fall in inches
    // intermediate variables for calculating output data
    double gallonsPerAcreInch = 27154.286;  // # of gallons per acre-inch
    double cubicMilesPerGallon = 0.00000000000090817; // # of cubic miles per gallon
    // calculation for output data
    float totalRain = (float)(affectedArea * rainFall * gallonsPerAcreInch * cubicMilesPerGallon * 100 / 100);
    // prints output data
    System.out.print(totalRain + " cubic miles");
  } // end of main method
} // end of class