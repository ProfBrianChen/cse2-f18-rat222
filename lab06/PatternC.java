/// Raymond To 10/11/2018 CSE 02 Pattern C
/// The purpose of this program is to display a pattern
/// based on input from the user
//
import java.util.Scanner; // Required in order to use Scanner class
public class PatternC{
  // main method required for every java program
  public static void main(String args []){
    int numRows = 0;
    String pattern = "";
    Scanner scan = new Scanner(System.in); // Declares and constructs instance odf scan to accept input
    while( numRows < 1 || numRows > 10){
      System.out.print("Enter an integer between 1 and 10: "); // Prints prompt for user to input
      boolean valid = false;
      while( valid == false){
        if( scan.hasNextInt()){
          numRows = scan.nextInt();
          valid = true;
        }
        else{
          scan.next();
          System.out.print("Enter an integer between 1 and 10: "); // Prints prompt for user to input
        }
      }
    }
    for(int i = numRows; i > 0; i--){
      for(int j = i + 7; j > 0; j--){
        pattern = pattern + " ";
      }
      for(int k = numRows + 1 - i; k > 0; k--){
        pattern = pattern + k;
      }
      System.out.println(pattern);
      pattern = "";
    }
  } // end of main method
} // end of class