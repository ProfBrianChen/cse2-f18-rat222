/// Raymond To 12/6/2018 CSE 02 Insertion Sort
/// The purpose of this program is
/// to sort an array using insertion sort
//
import java.util.Arrays;
public class InsertionSortLab10{
  public static void main(String[] args){
    int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
    int iterBest = insertionSort(myArrayBest);
    int iterWorst = insertionSort(myArrayWorst);
    System.out.println("The total number of of operation performed on the sorted array: " + iterBest);
    System.out.println("The total number of of operation performed on the reverse sorted array: " + iterWorst);
  }
  public static int insertionSort(int[] list){
    // Prints the initial array (you must insert another print out statement later in the code)
    System.out.println(Arrays.toString(list));
    // Initialize counter for iterations
    int iterations = 0;
    // For element list[i] in the array...
    for (int i = 1; i < list.length; i++){
      boolean change = false;
      // Update the iterations counter
      iterations++;
      // Insert list[i] into a sorted sublist list[0...i - 1] so that list[0...i] is sorted
      for(int j = i; j > 0; j--){
        if(list[j] < list[j - 1]){
          change = true;
          iterations++;
          int temp = list[j -1];
          list[j - 1] = list[j];
          list[j] = temp;
        }
        else{
          break;
        }
      }
      if(change){
        System.out.println(Arrays.toString(list));
      }
    }
    return iterations;
  }
}